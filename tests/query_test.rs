

use std::path::PathBuf;

use tree_sitter::{Parser, Query};
use treesitter_playground::params::{CVE_FILE_PATH, CVE_SOURCE_CODE_FILE_PATH, FIXED_CVE_SOURCE_CODE_FILE_PATH, QUERY_FOLDER_PATH};
use treesitter_playground::query_matching::{run_query, get_query_from_node};

#[test]
fn test_querry() {
    let mut parser = Parser::new();
    parser.set_language(tree_sitter_rust::language()).expect("Error loading Rust grammar");

    // cve
    let cve_code = std::fs::read_to_string(CVE_FILE_PATH).unwrap();
    let cve_tree = parser.parse(cve_code, None).expect("Error parsing the CVE");
    let cve_root_node = cve_tree.root_node();
    let query_str = get_query_from_node(&cve_root_node);
    println!("{}\n", cve_root_node.to_sexp());
    println!("{}\n", query_str);

    
    let query = Query::new(tree_sitter_rust::language(), query_str.as_str()).expect("Error creating query");

    // save the querry
    let mut cve_query_path = PathBuf::from(QUERY_FOLDER_PATH);
    cve_query_path.push("cve.scm");

    std::fs::write(cve_query_path, query_str).expect("Error writing the query");

    
    //println!("{}", cve_root_node.to_sexp());

    // file cve
    let file_cve = std::fs::read_to_string(CVE_SOURCE_CODE_FILE_PATH).unwrap();
    let file_cve_tree = parser.parse(&file_cve, None).unwrap();

    let results = run_query(&file_cve_tree, &query, &file_cve);

    println!("results: {:?}", results);
    assert!(results.len() > 0, "No results found");

    // file fixed cve
    let file_fixed_cve = std::fs::read_to_string(FIXED_CVE_SOURCE_CODE_FILE_PATH).unwrap();
    let file_fixed_cve_tree = parser.parse(&file_fixed_cve, None).unwrap();

    let results = run_query(&file_fixed_cve_tree, &query, &file_fixed_cve);

    println!("results: {:?}", results);
    assert!(results.len() == 0, "Results found");
}