use tree_sitter::Parser;
use treesitter_playground::hand_made_tree_matching::is_tree_inside;
use treesitter_playground::params::{CVE_FILE_PATH, CVE_SOURCE_CODE_FILE_PATH, FIXED_CVE_SOURCE_CODE_FILE_PATH};

#[test]
fn test_hand_made_tree_matching() {
    let mut parser = Parser::new();
    parser.set_language(tree_sitter_rust::language()).expect("Error loading Rust grammar");

    // cve
    let cve_code: String = std::fs::read_to_string(CVE_FILE_PATH).unwrap();
    let cve_tree = parser.parse(cve_code, None).unwrap();
    let cve_root_node = cve_tree.root_node();

    
    //println!("{}", cve_root_node.to_sexp());

    // file cve
    let file_cve = std::fs::read_to_string(CVE_SOURCE_CODE_FILE_PATH).unwrap();
    let file_cve_tree = parser.parse(&file_cve, None).unwrap();
    let file_cve_root_node = file_cve_tree.root_node();

    let begining_of_tree = is_tree_inside(&file_cve_root_node, &cve_root_node);

    println!("The cve is in the cve file : {:?}", begining_of_tree);
    assert!(begining_of_tree.len() > 0, "The cve is not in the cve file");

    // file fixed cve
    let file_fixed_cve = std::fs::read_to_string(FIXED_CVE_SOURCE_CODE_FILE_PATH).unwrap();
    let file_fixed_cve_tree = parser.parse(&file_fixed_cve, None).unwrap();
    let file_fixed_cve_root_node = file_fixed_cve_tree.root_node();

    let begining_of_tree = is_tree_inside(&file_fixed_cve_root_node, &cve_root_node);
    println!("The cve is in the fixed cve file : {:?}", begining_of_tree);

    assert!(begining_of_tree.len() == 0, "The cve is in the fixed cve file");

}