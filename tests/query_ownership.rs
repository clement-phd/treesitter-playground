use tree_sitter::{Parser, Query, QueryCursor, Tree};


const TREE_SOURCE_CODE : &'static str =include_str!("ressources/test_main.rs");
const QUERY_SOURCE_CODE : &'static str = include_str!("ressources/query.scm");

/// take the ownership of the string and drop it at the end
fn load_query(source_code : &str) -> (Query, Query) {
    let (left, right) = cut_query(source_code);
    let left_query = Query::new(tree_sitter_rust::language(), left.as_str()).expect("Error creating query");
    let right_query = Query::new(tree_sitter_rust::language(), right.as_str()).expect("Error creating query");

    (left_query, right_query)
}
fn cut_query(query_str : &str) -> (String, String) {
    let lines = query_str.lines().collect::<Vec<&str>>();
    let first_half = lines[0..lines.len()/2].join("\n");
    let second_half = lines[lines.len()/2..].join("\n");
    (first_half, second_half)
}
pub fn run_query(tree: &Tree, query: &Query, source_code: &str) {
    let query_cursor = &mut QueryCursor::new();
    
    let matches = query_cursor.matches(query, tree.root_node(), source_code.as_bytes());
    for m in matches {
        for c in m.captures {
            println!("found !");
        }
    }
}

/// This test aim to test the ownership of the query on the source code
#[test]
#[ignore]
fn test_querry_ownership() {
    let query_source_code = String::from(QUERY_SOURCE_CODE);
    let query_src = query_source_code.clone();


    let (left_query, right_query) = load_query(query_src.as_str());
    let tree_source_code = String::from(TREE_SOURCE_CODE);
    let mut parser = Parser::new();
    parser.set_language(tree_sitter_rust::language()).expect("Error loading Rust grammar");
    let tree = parser.parse(tree_source_code.as_str(), None).expect("Error parsing the source code");

    run_query(&tree, &left_query, &tree_source_code);
    run_query(&tree, &right_query, &tree_source_code);


}