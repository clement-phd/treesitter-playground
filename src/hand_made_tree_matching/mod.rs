use tree_sitter::{Node, Point};

/**
 * This function return true if the tree_to_test has the same structure than the ref_treeon this node
 * NOTE : Skip the test on the kind of the first node => source file
 */
pub fn is_similar_tree(ref_root: &Node, root_to_test: &Node) -> bool {

    let mut actual_ref_node = *ref_root;
    let mut actual_to_test_node = *root_to_test;

    loop{
        let ref_root = actual_ref_node;
        let root_to_test = actual_to_test_node;

        //println!("{} {} /-/ {} {}", ref_root.kind(), root_to_test.kind(), ref_root.child_count(), root_to_test.child_count());

        // breack on kind or child count
        if ref_root.kind() != root_to_test.kind() || ref_root.child_count() < root_to_test.child_count() {
            return false;
        }

        let mut cursor1 = ref_root.walk();
        let mut cursor2 = root_to_test.walk();

        let mut ref_children = ref_root.children(&mut cursor1);
        let mut to_test_children = root_to_test.children(&mut cursor2);

        
        'children_loop: loop {
            let ref_child = ref_children.next();
            let to_test_child = to_test_children.next();

            match (ref_child, to_test_child) {
                (Some(ref_child), Some(to_test_child)) => {
                    if !is_similar_tree(&ref_child, &to_test_child) {
                        return false;
                    }
                },
                (None, None) => {
                    break 'children_loop;
                },
                (None, Some(_)) => {
                    return false;
                }
                (Some(_), None) => {
                    break 'children_loop;
                }
            }
        }

        match (ref_root.next_sibling(), root_to_test.next_sibling()) {
            (Some(ref_node_sibling), Some(node_to_test_sibling)) => {
                actual_ref_node = ref_node_sibling;
                actual_to_test_node = node_to_test_sibling;
            },
            (None, None) => {
                return true;
            },
            (None, Some(_)) => {
                // if the ref_root has no more sibling but the root_to_test has, it's not similar
                return false;
            }
            (Some(_), None) => {
                // if the ref_root has more sibling but the root_to_test has not, root_to_test is fully inside ref_root
                return true;
            }
        }


    }
    
}

/**
 * test if the tree_to_test is inside the ref_tree and return all the line where the tree_to_test is inside the ref_tree
 */
pub fn is_tree_inside(ref_root: &Node, root_to_test: &Node) -> Vec<Point> {

    let mut begining_of_tree  = Vec::new();

    // if the test node is a source file, we skip it
    let root_to_test_sanitized = if root_to_test.kind() == "source_file" {
        root_to_test.child(0).unwrap()
    } else {
        *root_to_test
    };

 
    { // test the actual node
        if is_similar_tree(ref_root, &root_to_test_sanitized) {
            begining_of_tree.push(ref_root.start_position());
        }
    }

    let mut cursor = ref_root.walk();
    let mut ref_children = ref_root.children(&mut cursor);

    loop {
        let ref_child = ref_children.next();

        match ref_child {
            Some(ref_child) => {
                let child_begining_of_tree = is_tree_inside(&ref_child, &root_to_test_sanitized);
                if child_begining_of_tree.len() > 0 {
                    begining_of_tree.extend(child_begining_of_tree);
                }
            },
            None => {
               break;
            }
        }
    }

    return begining_of_tree;
}



#[cfg(test)]
mod tests {
    use tree_sitter::Parser;

    use super::*;

    fn is_code_similar(ref_string : &str, to_test : &str) -> bool {
        let mut parser = Parser::new();
        parser.set_language(tree_sitter_rust::language()).expect("Error loading Rust grammar");

        let ref_tree = parser.parse(ref_string, None).unwrap();
        let ref_root_node = ref_tree.root_node();
        println!("{}", ref_root_node.to_sexp());

        let to_test_tree = parser.parse(to_test, None).unwrap();
        let to_test_root_node = to_test_tree.root_node();
        println!("{}", to_test_root_node.to_sexp());

        return is_similar_tree(&ref_root_node, &to_test_root_node);
    }

    fn is_code_include(ref_string : &str, to_test : &str) -> bool {
        let mut parser = Parser::new();
        parser.set_language(tree_sitter_rust::language()).expect("Error loading Rust grammar");

        let ref_tree = parser.parse(ref_string, None).unwrap();
        let ref_root_node = ref_tree.root_node();
        println!("{}", ref_root_node.to_sexp());

        let to_test_tree = parser.parse(to_test, None).unwrap();
        let to_test_root_node = to_test_tree.root_node();
        println!("{}", to_test_root_node.to_sexp());

        return is_tree_inside(&ref_root_node, &to_test_root_node).len() > 0;
    }

    /////////////////////////////////////// tree similar ///////////////////////////////////////

    #[test]
    fn test_is_similar_tree_same_code1() {
        let main_code = "let a = 1;";
        assert!(is_code_similar(main_code, main_code), "main_code and main_code are not similar");
    }
    #[test]
    fn test_is_similar_tree_same_code2() {
        let main_code = "let a = 1;
        let b = 2;";
        assert!(is_code_similar(main_code, main_code), "main_code and main_code are not similar");
    }

    #[test]
    fn test_is_similar_tree_different_code_invalid_1() {
        let code1 = "let a = 1;";
        let code2 = "let b = c as u64;";
        assert!(!is_code_similar(code1, code2), "code1 and code2 are similar");
    }

    #[test]
    fn test_is_similar_tree_different_code_valid_1() {
        let code1 = "let a = x as u32; let a = 1;";
        let code2 = "let b = c as u64;";
        assert!(is_code_similar(code1, code2), "code1 and code2 are not similar");
    }


    #[test]
    fn test_is_include_tree_same_code() {
        let main_code = "let a = 1;";
        assert!(is_code_include(main_code, main_code), "main_code and main_code are not similar");
    }

    /////////////////////////////////////// tree include ///////////////////////////////////////

    #[test]
    fn test_is_include_tree_valid_1() {
        let code1 = "let a = 1 as u64;
        let b = 2;";
        let code2 = "let b = 2;";
        assert!(is_code_include(code1, code2), "code1 and code2 are not similar");
    }

    #[test]
    fn test_is_include_tree_valid_2() {
        let code1 = "let a = 1 as u64;
        let b = 2;";
        let code2 = "let b = 2 as u64;";
        assert!(is_code_include(code1, code2), "code1 and code2 are not similar");
    }

    /////////////////////////////////////// tree sitter ///////////////////////////////////////

    #[test]
    fn tree_sitter_test() {
        // try if I can count the sibling well
        let code1 = "let a = 1; let b = 2 as u64;";

        let mut parser = Parser::new();
        parser.set_language(tree_sitter_rust::language()).expect("Error loading Rust grammar");

        let tree = parser.parse(code1, None).unwrap();

        let root_node = tree.root_node();

        assert_eq!(root_node.child_count(), 2);

        let mut cursor = root_node.walk();

        cursor.goto_first_child();

        assert_eq!(cursor.node().kind(), "let_declaration");

        assert!(cursor.goto_next_sibling());

        assert_eq!(cursor.node().kind(), "let_declaration");


        assert!(!cursor.goto_next_sibling());


        assert!(cursor.goto_first_child());

        assert_eq!(cursor.node().kind(), "let");

        assert!(cursor.goto_next_sibling());

    }

    /////////////////////////////////////// dot ///////////////////////////////////////
    

    #[test]
    #[cfg(feature = "dot_tests")]
    fn test_to_dot_file() {
        use crate::{params::{CVE_FILE_PATH, GRAPH_FOLDER_PATH}, to_dot_file};
        use std::path::PathBuf;

        let mut parser = Parser::new();
        parser.set_language(tree_sitter_rust::language()).expect("Error loading Rust grammar");
        
        let cve_code = std::fs::read_to_string(CVE_FILE_PATH).unwrap();
        let cve_tree = parser.parse(cve_code, None).unwrap();

        let mut cve_graph_path = PathBuf::from(GRAPH_FOLDER_PATH);
        cve_graph_path.push("cve.dot");

        to_dot_file(&cve_tree, cve_graph_path.to_str().unwrap());
    }
}