use std::fs::File;

use tree_sitter::Tree;

pub mod hand_made_tree_matching;
pub mod query_matching;
pub mod params;

/**
 * Print the tree in a dot file
 */
pub fn to_dot_file(tree: &Tree, path: &str) {
    let file = File::create(path).unwrap();
    tree.print_dot_graph(&file);
}