pub const CVE_FILE_PATH : &'static str = "ressources/code/cve/CVE-2023-22895.txt";

pub const CVE_SOURCE_CODE_FILE_PATH : &'static str = "ressources/code/cve_source_code/CVE-2023-22895.rs";
pub const FIXED_CVE_SOURCE_CODE_FILE_PATH : &'static str = "ressources/code/fixed_cve_source_code/CVE-2023-22895.rs";

pub const GRAPH_FOLDER_PATH : &'static str = "ressources/graphs";
pub const QUERY_FOLDER_PATH : &'static str = "ressources/queries";