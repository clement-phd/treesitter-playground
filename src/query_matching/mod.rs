use tree_sitter::{Node, Point};



/**
 * get the general querry from a node (removing source_code for example)
 */
pub fn get_query_from_node(node: &Node) -> String{
    let base_query = {
        if node.kind() == "source_file" {
            
            let mut actual_node = node.child(0).unwrap();
            let mut query = String::new();
            loop {
                query.push_str(format!("{}", actual_node.to_sexp()).as_str());

                match actual_node.next_sibling() {
                    Some(next_node) => {
                        actual_node = next_node;
                        query.push_str(" . ");
                    },
                    None => {
                        break;
                    }
                }
            }
            query
        }else{
            node.to_sexp()
        }

    };

    let query = format!("(_ {} ) @cve", base_query);

    return query;
}


pub fn run_query(tree: &tree_sitter::Tree, query: &tree_sitter::Query, source_code: &str) -> Vec<(Point, Point)> {
    let mut matches_vec = Vec::new();
    let query_cursor = &mut tree_sitter::QueryCursor::new();
    
    let matches = query_cursor.matches(query, tree.root_node(), source_code.as_bytes());

    for m in matches {
        for c in m.captures {
            matches_vec.push((c.node.start_position(), c.node.end_position()));
        }
    }

    return matches_vec;
}
