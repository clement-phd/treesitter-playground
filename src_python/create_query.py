from tree_sitter import Language, Parser

# !!!!!!!!!!!!!!!!!!!!!!!! Build languages !!!!!!!!!!!!!!!!!!!!!!!!

LANGUAGE_FOLDER_PATH = "ressources/languages_def/"

MY_LANGUAGE_PATH = "build/my-languages.so"

Language.build_library(
    # Store the library in the `build` directory
    MY_LANGUAGE_PATH,
    # Include one or more languages
    [LANGUAGE_FOLDER_PATH + "tree-sitter-rust", LANGUAGE_FOLDER_PATH + "tree-sitter-c"],
)

RUST_LANGUAGE = Language(MY_LANGUAGE_PATH, "rust")
C_LANGUAGE = Language(MY_LANGUAGE_PATH, "c")

# !!!!!!!!!!!!!!!!!!!!!!!! Parse code !!!!!!!!!!!!!!!!!!!!!!!!

CODE_FILE_PATH = "ressources/code/cve/CVE-2023-22895.txt"



if __name__ == "__main__":
    parser = Parser()
    parser.set_language(RUST_LANGUAGE)
    cve_code : str
    with open(CODE_FILE_PATH, "r") as f:
        cve_code = f.read()
    
    tree = parser.parse(cve_code)
    root_node = tree.root_node
    print(root_node.sexp())
