from tree_sitter import Language, Parser

# !!!!!!!!!!!!!!!!!!!!!!!! Build languages !!!!!!!!!!!!!!!!!!!!!!!!

LANGUAGE_FOLDER_PATH = "ressources/languages_def/"

MY_LANGUAGE_PATH = "build/my-languages.so"

Language.build_library(
    # Store the library in the `build` directory
    MY_LANGUAGE_PATH,
    # Include one or more languages
    [LANGUAGE_FOLDER_PATH + "tree-sitter-rust", LANGUAGE_FOLDER_PATH + "tree-sitter-c"],
)

RUST_LANGUAGE = Language(MY_LANGUAGE_PATH, "rust")
C_LANGUAGE = Language(MY_LANGUAGE_PATH, "c")

# !!!!!!!!!!!!!!!!!!!!!!!! Parse code !!!!!!!!!!!!!!!!!!!!!!!!

CODE_FILE_PATH = "ressources/code/cve_source_code/CVE-2023-22895.rs"

CVE_QUERY_PATH = "ressources/queries/cve.scm"



if __name__ == "__main__":
    parser = Parser()
    parser.set_language(RUST_LANGUAGE)
    cve_query : str
    file_content : str

    with open(CVE_QUERY_PATH, "r") as f:
        cve_query = f.read()

    with open(CODE_FILE_PATH, "r") as f:
        file_content = f.read()

    query = RUST_LANGUAGE.query(cve_query)
    tree = parser.parse(file_content.encode())
    
    captures = query.captures(tree.root_node)

    assert len(captures) == 2

    print(captures[0])
    print(captures[1])