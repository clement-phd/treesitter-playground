# treesitter-playground

Try to parse the [CVE-2023-22895](https://www.opencve.io/cve/CVE-2023-22895).

## Integration test
 To run an integration test (inside the `tests/` folder) in rust : `cargo test --test [name of the test file] -- --nocapture`

## Hand made tree matching

```shell
cargo test --test hand_made_tree_matching_test -- --nocapture
```

## tree sitter querry

```shell
cargo test --test query_test -- --nocapture
```

### Display a graph

The outputs will be in the `ressources/graphs/` folder.

```shell
cargo test --features dot_tests

dot -Gsize=256! -Tpng ressources/graphs/cve.dot > ressources/graphs/cve.png
```

## Python library

Use the language definition inside `ressources/languages_def`. You must run the following (to have the language) inside the folder :

```shell
git clone https://github.com/tree-sitter/tree-sitter-rust
git clone https://github.com/tree-sitter/tree-sitter-c
```
Then call (at the root of the project, to not destroy he path) :

```shell
python src_python/main.py
```

# LIMITATIONS

> Theyr is no rule in the query language of tree sitter to specify sibling relation (or sequential order, this is equivalent). Their are 2 solutions : put 1 rules for every nodes with many children, or do it programmatically